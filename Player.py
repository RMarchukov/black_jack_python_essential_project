import abc

class AbstractPlayer(abc.ABC):
    def __init__(self):
        self.cards = []
        self.bet = 0
        self.full_points = 0
        self.money = 0

    def set_money(self):
        self.money = int(input('How much money do you want to put in your bank? : '))


    def change_points(self):
        self.full_points = sum([card.points for card in self.cards])

    def take_card(self, card):
        self.cards.append(card)
        self.change_points()

    def print_cards(self):
        print(self, end=' ')
        for card in self.cards:
            print(card, end=' | ')
        print()
        print('Total points:', self.full_points)
        print('------------------------------------------------')

    @abc.abstractmethod
    def ask_card(self):
        pass

    @abc.abstractmethod
    def change_bet(self, max_bet, min_bet):
        pass


class Player(AbstractPlayer):
    def change_bet(self, max_bet, min_bet):
        while True:
            value = int(input('Make your bet(1-100): '))
            if max_bet >= value >= min_bet:
                self.bet = value
                self.money -= self.bet
                break
        print('Your bet is:', self.bet)



    def ask_card(self):
        choice = input('Do you want to take a new card?(y/n): ')
        if choice == 'y':
            return True
        else:
            return False

    def __str__(self):
        return 'Your cards:'


class Dealer(AbstractPlayer):
    max_points = 17

    def change_bet(self, max_bet, min_bet):
        raise Exception('This type is dealer so it has no bets')

    def ask_card(self):
        if self.full_points < self.max_points:
            return True
        else:
            return False

    def __str__(self):
        return "Dealer's cards:"