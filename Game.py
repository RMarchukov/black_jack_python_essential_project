import Player
from Deck import Deck

class Game:
    def __init__(self):
        self.player = Player.Player()
        self.dealer = Player.Dealer()
        self.deck = Deck()
        self.max_bet, self.min_bet = 100, 1

    @staticmethod
    def _ask_starting(message):
        while True:
            choice = input(message)
            if choice == 'n':
                return False
            elif choice == 'y':
                return True

    def _ask_bet(self):
        self.player.change_bet(self.max_bet, self.min_bet)

    def first_descr(self):
        for _ in range(2):
            card = self.deck.get_card()
            self.player.take_card(card)

        card = self.deck.get_card()
        self.dealer.take_card(card)
        self.dealer.print_cards()

    def check_stop(self):
        if self.player.full_points > 21:
            return True
        else:
            return False

    def ask_cards(self):
        while self.player.ask_card():
            card = self.deck.get_card()
            self.player.take_card(card)
            is_stop = self.check_stop()
            if is_stop:
                if self.player.full_points > 21:
                    print('You have lost')
                break
            self.player.print_cards()


    def check_winner(self):
        if self.dealer.full_points > 21 >= self.player.full_points:
            self.player.money += self.player.bet * 2
            print('Dealer has lost')
        else:
            if 21 > self.player.full_points == self.dealer.full_points < 21:
                self.player.money += self.player.bet
                print('Player points = dealer points')
            elif 21 >= self.player.full_points > self.dealer.full_points:
                self.player.money += self.player.bet * 2
                print('You are winner!')
            elif self.player.full_points < self.dealer.full_points:
                print('You have lost')

    def play_with_dealer(self):
        while self.dealer.ask_card():
            card = self.deck.get_card()
            self.dealer.take_card(card)
        self.dealer.print_cards()


    def start_game(self):
        message = 'Do you want to continue?(y/n): '
        self.player.set_money()

        while self.player.money > 0:
            if not self._ask_starting(message):
                break
            self._ask_bet()
            self.first_descr()
            self.player.print_cards()
            self.ask_cards()
            self.play_with_dealer()
            self.check_winner()
            self.player.print_cards()
            print(f'Your bank = {self.player.money}')
            self.player.cards = []
            self.dealer.cards = []

        print(f'You have finished the game. Your bank = {self.player.money}')
